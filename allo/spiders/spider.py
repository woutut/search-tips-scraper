#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
import json
from allo.items import AlloItem
import sqlite3
from tqdm import tqdm


class SearchSpider(scrapy.Spider):

    name = "tips"
    allowed_domains = ['allo.ua']

    # Get all keywords if first run, get unprocessed if repeated run
    def get_unprocessed_keywords_from_db(self):
        # Connect to db
        self.connection = sqlite3.connect(self.settings.get('DB_NAME'))
        self.cur = self.connection.cursor()
        # Get unprocessed keywords
        self.cur.execute("SELECT keyword FROM keywords WHERE status = 0;")
        keywords = self.cur.fetchall()
        # Close db connection
        self.cur.close()
        self.connection.close()
        # Return keywords
        return [x[0] for x in keywords]

    def start_requests(self):

        # Prepare keywords (slicing with CLI parameter if needed)
        keywords = (
            self.get_unprocessed_keywords_from_db()[
                : self.settings.getint('TIPS_COUNT')
            ]
            if self.settings.getint('TIPS_COUNT')
            else self.get_unprocessed_keywords_from_db()
        )

        # Process keywords
        for k in tqdm(keywords, desc='Process keywords'):
            # Prepare requests
            request = scrapy.Request(
                url='https://allo.ua/ru/catalogsearch/ajax/suggest/?q={}'.format(k),
                callback=self.parse,
            )
            # Append keyword data
            request.meta['keyword'] = k
            yield request

    def parse(self, response):
        # Prepare data
        item = AlloItem(
            keyword=response.meta['keyword'],
            status=True,
            search_tips=json.loads(response.body).get('query', [])
            if json.loads(response.body)
            else [],
        )
        yield item
