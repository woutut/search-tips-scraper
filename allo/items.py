# -*- coding: utf-8 -*-
import scrapy


class AlloItem(scrapy.Item):
    # define the fields for your item here like:
    status = scrapy.Field()
    keyword = scrapy.Field()
    search_tips = scrapy.Field()