#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
from itertools import product
from scrapy import signals


# Prepare all keywords and insert in database
# with unprocessed (0) status (if first run)
class PrepareData(object):
    def __init__(self, letters, db_name, keywords_table, tips_table):
        self.letters = letters
        self.db_name = db_name
        self.keywords_table = keywords_table
        self.tips_table = tips_table

    @classmethod
    def from_crawler(cls, crawler):
        # Get data from settings
        letters = crawler.settings.get('LETTERS')
        db_name = crawler.settings.get('DB_NAME')
        keywords_table = crawler.settings.get('KEYWORDS_TABLE')
        tips_table = crawler.settings.get('TIPS_TABLE')

        # instantiate the extension object
        ext = cls(letters, db_name, keywords_table, tips_table)

        # connect the extension object to signals
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)

        # return the extension object
        return ext

    # Get all combinations of letters
    @staticmethod
    def generate_keywords(letters):
        # Generate all 2-letter combinations
        keywords_2 = [''.join(i) for i in product(letters, repeat=2)]
        # Generate all 3-letter combinations
        keywords_3 = [''.join(i) for i in product(letters, repeat=3)]
        # Return combined list
        return keywords_2 + keywords_3

    # Connect to db when the spider is opened
    def spider_opened(self, spider):
        # Connect to database
        self.connection = sqlite3.connect(self.db_name)
        self.cur = self.connection.cursor()
        # Check if keywords db exists (monitor repeated runs)
        self.cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='{}'".format(
                self.keywords_table
            )
        )
        # If not exists - first run
        if not self.cur.fetchone():
            # Prepare keywords
            keywords = self.generate_keywords(self.letters)
            # Create keywords table
            self.cur.execute(
                """
                CREATE TABLE {}
                (id INTEGER PRIMARY KEY AUTOINCREMENT,
                keyword TEXT, status INTEGER)
                """.format(
                    self.keywords_table
                )
            )
            # Insert keywords
            for k in keywords:
                self.cur.execute(
                    """
                    INSERT INTO {}(keyword, status)
                    VALUES (?, ?)
                    """.format(
                        self.keywords_table
                    ), (k, 0)
                )
            # Commit changes
            self.connection.commit()
        # If exists - repeated run, all data already prepared
        else:
            pass
        # Close db connection
        self.cur.close()
        self.connection.close()
