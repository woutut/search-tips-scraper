# -*- coding: utf-8 -*-
import sqlite3


# SQLite3 pipleine
class AlloPipeline(object):
    def __init__(self, db_name, keywords_table, tips_table):
        self.db_name = db_name
        self.keywords_table = keywords_table
        self.tips_table = tips_table

    @classmethod
    def from_crawler(cls, crawler):
        # Get data from settings
        db_name = crawler.settings.get('DB_NAME')
        keywords_table = crawler.settings.get('KEYWORDS_TABLE')
        tips_table = crawler.settings.get('TIPS_TABLE')
        return cls(db_name, keywords_table, tips_table)

    # Connect to db when the spider is opened
    def open_spider(self, spider):
        self.connection = sqlite3.connect(self.db_name)
        self.cur = self.connection.cursor()
        # Create table for tips if not exists
        self.cur.execute(
            """
            CREATE TABLE IF NOT EXISTS {}
            (id INTEGER PRIMARY KEY AUTOINCREMENT,
            keyword TEXT, tips TEXT)
            """.format(
                self.tips_table
            )
        )
        self.connection.commit()

    # Close db connection when the spider is closed
    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    # Process search tips data
    def process_item(self, item, spider):
        # Add tips to DB
        for tip in item['search_tips']:
            self.cur.execute(
                """
                INSERT INTO {}(keyword, tips)
                VALUES (?, ?)
                """.format(
                    self.tips_table
                ), (item['keyword'], tip)
            )
        # Change keyword status to processed (1)
        self.cur.execute(
            """
            UPDATE {}
            SET status = 1
            WHERE keyword = '{}'
            """.format(
                self.keywords_table, item['keyword']
            )
        )
        # Commit changes to DB
        self.connection.commit()
        return item
