# -*- coding: utf-8 -*-
from scrapy import signals
from w3lib.http import basic_auth_header
from random import choice
from re import match


class AlloSpiderMiddleware(object):
    # Not all methods need to be defined. If a method is not defined,
    # scrapy acts as if the spider middleware does not modify the
    # passed objects.

    @classmethod
    def from_crawler(cls, crawler):
        # This method is used by Scrapy to create your spiders.
        s = cls()
        crawler.signals.connect(s.spider_opened, signal=signals.spider_opened)
        return s

    def process_spider_input(self, response, spider):
        # Called for each response that goes through the spider
        # middleware and into the spider.

        # Should return None or raise an exception.
        return None

    def process_spider_output(self, response, result, spider):
        # Called with the results returned from the Spider, after
        # it has processed the response.

        # Must return an iterable of Request, dict or Item objects.
        for i in result:
            yield i

    def process_spider_exception(self, response, exception, spider):
        # Called when a spider or process_spider_input() method
        # (from other spider middleware) raises an exception.

        # Should return either None or an iterable of Response, dict
        # or Item objects.
        pass

    def process_start_requests(self, start_requests, spider):
        # Called with the start requests of the spider, and works
        # similarly to the process_spider_output() method, except
        # that it doesn’t have a response associated.

        # Must return only requests (not items).
        for r in start_requests:
            yield r

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)


# Proxies middleware
class AlloDownloaderMiddleware(object):
    def __init__(self, proxies):
        self.proxies = proxies

    @classmethod
    def from_crawler(cls, crawler):
        # Get proxies from settings file
        proxies = crawler.settings.getlist('PROXIES')
        return cls(proxies)

    @staticmethod
    def process_proxy(proxy):
        # Check if proxy data is correct
        if not match('(.+:.+@|)[0-9.]+:[0-9]+', proxy):
            raise Exception('Invalid proxy: {}'.format(proxy))
        # Prepare dict for proxy data
        proxy_data = {'host': None, 'port': None, 'user': None, 'pass': None}
        # Check if auth included
        if '@' in proxy:
            auth, conn = proxy.split('@')
            proxy_data['user'], proxy_data['pass'] = auth.split(':')
            proxy_data['host'], proxy_data['port'] = conn.split(':')
        # If not
        else:
            proxy_data['host'], proxy_data['port'] = proxy.split(':')
        # Return proxy data
        return proxy_data

    # Request through random proxy
    def process_request(self, request, spider):
        # If proxies are set
        if self.proxies:
            # Get random proxy
            proxy = self.process_proxy(choice(self.proxies))
            # Request through proxy
            request.meta['proxy'] = "http://{}:{}".format(proxy['host'], proxy['port'])
            # If auth needed
            if proxy.get('user') and proxy.get('pass'):
                request.headers['Proxy-Authorization'] = basic_auth_header(
                    proxy['user'], proxy['pass']
                )
            return None
        # If no proxies - do nothing
        else:
            return None

    def process_response(self, request, response, spider):
        # Called with the response returned from the downloader.

        # Must either;
        # - return a Response object
        # - return a Request object
        # - or raise IgnoreRequest
        return response

    def process_exception(self, request, exception, spider):
        # Called when a download handler or a process_request()
        # (from other downloader middleware) raises an exception.

        # Must either:
        # - return None: continue processing this exception
        # - return a Response object: stops process_exception() chain
        # - return a Request object: stops process_exception() chain
        pass

    def spider_opened(self, spider):
        spider.logger.info('Spider opened: %s' % spider.name)
