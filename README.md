# Search Tips Scraper

Scrapy bot for scraping search tips (allo.ua). Generates maximum combinations of 2-3 letters and scrape search tips through AJAX API. Each request uses a random HTTP proxy (if 2+ proxies specified).

## Usage

- Activate python3 venv and install requirements.txt.

- Edit `allo/settings.py` to:
    - adjust concurrent requests count;
    - adjust delays between requests (or auto throttling options);
    - choose database/tables names;
    - change letters to generate combinations;
    - add HTTP proxies (if no proxy added — spider will use the main IP);

- Run `scrapy crawl tips`.

###  Persistent scraping

All scraping results are stored in a SQLite3 database. Each time you run or rerun the spider, it searches for unprocessed letter combinations in an existing database, so you'll never scrape the same combination twice.

If no database exists, bot creates a new database and generates/insert all possible combinations with unprocessed (0) status.

### Additional information

- You can choose how many combination search tips scrape with a single run:
```
scrapy crawl tips -s TIPS_COUNT=10
```
- You can output scraping results to json/jsonlines file (jsonlines prefered because of rerun possibility):
```
scrapy crawl tips -o allo.jsonlines
```
- Bot uses tqdm for updating progress bar, but it looks cleaner without Scrapy logs:
```
scrapy crawl tips --nolog
```